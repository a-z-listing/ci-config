This repository is for use with GitLab-CI. The configurations here are for building and releasing WordPress plugins either to WordPress.org or a premium plugin to Amazon S3 hosting and a WooCommerce with [API Manager](https://woocommerce.com/products/woocommerce-api-manager/) website.

## Importing to .gitlab-ci.yml
Add the following at the top of your `.gitlab-ci.yml` to include the configurations into your CI pipeline:

```yaml
  - project: a-z-listing/ci-config
    file: /build-and-test.yml
  - project: a-z-listing/ci-config
    file: /release-WordPress.org.yml
```

Alternatively for a premium plugin:

```yaml
  - project: a-z-listing/ci-config
    file: /build-and-test.yml
  - project: a-z-listing/ci-config
    file: /release-premium.yml
```

## Configuration
You must supply secrets/variables for the following in your repository:

### All plugins
- `SLUG` - the slug of your WordPRess plugin. Default is `${CI_PROJECT_NAME}`

### WordPress.org hosted plugins
- `SVN_USERNAME` - your WordPress.org SVN username. No default - this _must_ be set. Ideally set this as a masked variable.
- `SVN_PASSWORD` - your WordPress.org SVN password. No default - this _must_ be set. Ideally set this as a masked variable.
- `ASSETS_DIR` - the path within your repository to the assets directory for use in the plugin's listing page on WordPress.org. This is copied to the WordPress.org SVN in `./assets` (_outside_ of the `trunk` and `tags` directories). Default is `.wordpress.org`.

### Premium plugins sold via WooCommerce+API Manager
- `ZIPFILE_NAME` - the name of your zipfile. Default is `${CI_PROJECT_NAME}-${CI_COMMIT_TAG}.zip`.
- `WOOCOMMERCE_HOST` - the URL to your WordPress website's homepage. Default is `https://example.com` and _must_ be overridden.
- `PRODUCT_ID`  - the product ID of the plugin in your WordPress website. No default - this _must_ be set.
- `S3_BUCKET_NAME` - the name of your AWS S3 bucket. No default - this _must_ be set. Ideally set this as a masked variable.
- `AWS_DEFAULT_REGION` - the region of your AWS S3 bucket. No default - this _must_ be set. Ideally set this as a masked variable.
- `AWS_ACCESS_KEY_ID` - your AWS Access Key ID. No default - this _must_ be set. Ideally set this as a masked variable.
- `AWS_SECRET_ACCESS_KEY` your AWS Secret Access Key. No default - this _must_ be set. Ideally set this as a masked variable.

