ARG PHP_VERSION=7.4
ARG NODEJS_VERSION=14.15.3

FROM node:${NODEJS_VERSION}-alpine as node

FROM php:${PHP_VERSION}-alpine

LABEL maintainer="Dani Llewellyn <dani@bowlhat.net>"

COPY --from=node /usr/local /usr/local

RUN apk upgrade --no-cache -U
RUN apk add --no-cache \
        bash \
        curl \
        freetype \
        git \
        gnupg \
        icu-libs \
        libjpeg \
        libmcrypt \
        libstdc++ \
        libxml2 \
        mysql-client \
        oniguruma \
        openssl \
        rsync \
        subversion \
        unzip \
        wget

RUN apk add --no-cache --virtual build-dependencies \
        freetype-dev \
        icu-dev \
        jpeg-dev \
        libmcrypt-dev \
        libxml2-dev \
        libzip-dev \
        oniguruma-dev \
        openssl-dev \
    && \
    docker-php-ext-install -j$(nproc) iconv intl xml soap opcache pdo pdo_mysql mysqli mbstring gd zip && \
    apk del build-dependencies

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
